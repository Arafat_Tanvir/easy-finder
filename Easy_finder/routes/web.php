<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//conform and pendding
Route::post('/bechelor_confirm/{bechelor_confirm}/completed', [
    'uses' => 'BechelorBookingController@completed',
    'as' => 'bechelor_confirm.completed'
]);//hello every one

Route::middleware(['auth'])->group(function () {

	Route::resource('bechelor_room','Bechelor_RoomController');
	Route::resource('comment','BechelorCommentController');
	Route::resource('profile','ProfileController');
	Route::resource('booking','BechelorBookingController');

});

//bechelor Booking route
Route::get('/bechelor/{bechelor}/booking', [
    'uses' => 'BechelorBookingController@booking',
    'as' => 'bechelor.booking'
]);

Route::get('/cities/ajax/{id}', [
	'uses' => 'Bechelor_RoomController@cities_ajax',
	'as' => 'cities'
]);

Route::get('/thanas/ajax/{id}', [
	'uses' => 'Bechelor_RoomController@thanas_ajax',
	'as' => 'thanas'
]);


//search city ,thana and ward
Route::get('/search', [
	'uses' => 'ThanaController@search',
	'as' => 'search'
]);
// 
Route::post('/search_post/search_post', [
	'uses' => 'ThanaController@search_post',
	'as' => 'search_post.search_post'
]);

//using thana id show ....

Route::get('/bechelor/show/{id}', [
	'uses' => 'ThanaController@show',
	'as' => 'bechelor.show'
]);
// show post using user id 
Route::get('/bechelor_post/by_user_id/{id}', [
	'uses' => 'ProfileController@by_user_id',
	'as' => 'bechelor_post.by_user_id'
]);


//user_booking
Route::get('/bechelor_booking/user_booking/{id}', [
	'uses' => 'BechelorBookingController@user_booking',
	'as' => 'bechelor_booking.user_booking'
]);


//
Route::get('/room_details/room_details/{id}', [
	'uses' => 'Bechelor_RoomController@room_details',
	'as' => 'room_details.room_details'
]);

//show Request
Route::get('/show_request/show_request/{id}', [
	'uses' => 'BechelorBookingController@show_request',
	'as' => 'show_request.show_request'
]);

//user details 
Route::get('/user_details/show/{id}', [
	'uses' => 'HomeController@user_show',
	'as' => 'user_details.show'
]);