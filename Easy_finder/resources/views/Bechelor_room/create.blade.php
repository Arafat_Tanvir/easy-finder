@extends('layouts.app')
@section('content')
  <script type="text/javascript" src="{{ url('')}}/assets/js/jquery-3.2.1.min.js"></script>
  <div class="content">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 offset-xs-2 offset-sm-2 offset-md-2">
          @if((Session::get('message')))
              <div class="alert alert-success alert-dismissable">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message')}}
                </div>
              </div>
          @endif
          <div class="card">
            <div class="card-header" style="text-align: center; background-color: #0321fd; color: #ffffff">
              <strong>Bechelor Room Create/Post Form</strong>
            </div>
            <div class="card-body card-block">
              <form role="form" action="{{ route('bechelor_room.store')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field()}}
                <div class="form-group">
                  <label for="date">Select Status</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-calendar fa-lg fa-spin"></i></div>
                  <select name="date" id="date" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===whem Seat OR Room Available===</option>
                    <option value="1ST JANUARY" >1ST JANUARY</option>
                    <option value="1ST FEBRUARY" >1ST FEBRUARY </option>
                    <option value="1ST MARCH" > 1ST MARCH </option>
                    <option value="1ST APRIL" > 1ST APRIL </option>
                    <option value="1ST MAY" > 1ST MAY </option>
                    <option value="1ST JUNE" >1ST JUNE </option>
                    <option value="1ST JULY" >1ST JULY </option>
                    <option value="1ST AUGUST" >1ST AUGUST </option>
                    <option value="1ST SEPTEMBER" >1ST SEPTEMBER </option>
                    <option value="1ST OCTOBER" >1ST OCTOBER </option>
                    <option value="1ST NOVEMBER" >1ST NOVEMBER </option>
                    <option value="1ST DECEMBER" >1ST DECEMBER </option>
                    
                  </select>
                  <p class="help-block">{{ ($errors->has('status')) ? $errors->first('status') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Select Status</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-graduation-cap fa-spin fa-lg"></i></div>
                  <select name="status" id="status" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===Choose Status===</option>
                    <option value="Student" >Student</option>
                    <option value="Job_holder" >Job Holder</option>
                    <option value="Teacher" >Teacher</option>
                    <option value="Other" >Other</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('status')) ? $errors->first('status') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Select Gender</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green" ><i class="fa fa-male"><input type="radio" name="gender" value="male">Male</i></div>
                    <div class="input-group-addon" style="margin-left: 20px; color: green"><i class="fa fa-male"><input type="radio" name="gender" value="female">Female</i></div>

                  <p class="help-block">{{ ($errors->has('gender')) ? $errors->first('gender') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Select Religion</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-heart fa-spin fa-lg"></i></div>
                  <select name="religion" id="religion" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===Choose Religion===</option>
                    <option value="Islam" >Islam</option>
                    <option value="Hinduism" >Hinduism</option>
                    <option value="Buddhism" >Buddhism</option>
                    <option value="Christianity" >Christianity</option>
                    <option value="Other" >Other</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('religion')) ? $errors->first('religion') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="married">Select Married</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class=" fa fa-venus-double fa-lg"><input type="radio" name="married" value="married">Yes</i></div>

                    <div class="input-group-addon" style="margin-left: 20px;color: green"><i class="fa fa-venus fa-lg"><input type="radio" name="married" value="Unmarried">No</i></div>
                  
                  <p class="help-block">{{ ($errors->has('married')) ? $errors->first('married') : ''}}</p>
                </div>
                </div>
                <div class="form-group">
                  <label>How Many Room Available</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-home fa-lg fa-spin"></i></div>
                  <select name="room" id="room" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===Choose Room===</option>
                    <option value="1" >1 Room</option>
                    <option value="2" >2 Room</option>
                    <option value="3" >3 Room</option>
                    <option value="4" >4 Room</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('room')) ? $errors->first('room') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>How Many Seat Available</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-clone fa-lg fa-spin"></i></div>
                  <select name="seat" id="seat" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===Choose Seat===</option>
                    <option value="1" >1 Seat</option>
                    <option value="2" >2 Seat</option>
                    <option value="3" >3 Seat</option>
                    <option value="4" >4 Seat</option>
                    <option value="5" >5 Seat</option>
                    <option value="6" >6 Seat</option>
                    <option value="7" >7 Seat</option>
                    <option value="8" >8 Seat</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('seat')) ? $errors->first('seat') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="room_type">Select Room Type</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-home fa-lg"><input type="radio" name="room_type" value="Semiphaka">Semiphaka</i></div>
                    <div class="input-group-addon" style="color: green; margin-left: 20px"><i class="fa fa-home fa-lg"><input type="radio" name="room_type" value="Flat">Flat</i></div>
                  <p class="help-block">{{ ($errors->has('room_type')) ? $errors->first('room_type') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Per Seat Rent</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-money fa-spin fa-lg"></i></div>
                  <input type="number" name="room_rent" class="form-control" value="{{ old('room_rent')}}" required>
                  <p class="help-block">{{ ($errors->has('room_rent')) ? $errors->first('room_rent') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Seat Image</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-image fa-lg fa-spin"></i></div>
                  <input type="file" name="image" value="{{ old('image')}}" required>
                  <p class="help-block">{{ ($errors->has('image')) ? $errors->first('image') : ''}}</p>
                </div>
              </div>
                <div class="form-group">
                  <label for="facilities">Facilities<div class="input-group-addon" style="color: green"><i class="fa fa-info-circle fa-spin"></i></div></label><br>
                  <input type="checkbox" name="facilities[]" value="Fully Decorated"/>Fully Decorated</br>
                  <input type="checkbox" name="facilities[]" value="Attach Bathroom" />Attach Bathroom</br>
                  <input type="checkbox" name="facilities[]" value="A Balcony along with the room" />A Balcony along with the room</br>
                  <input type="checkbox" name="facilities[]" value="24 Hours Water and Gass Supply" />24 Hours Water and Gass Supply</br>
                  <input type="checkbox" name="facilities[]" value="A quiet and Lovely environment" />A quiet and Lovely environment</br>
                  <input type="checkbox" name="facilities[]" value="Wifi" />Wifi</br>
                  <input type="checkbox" name="facilities[]" value="Freeze"/>Freeze</br>
                  <input type="checkbox" name="facilities[]" value="Security Guard"/>Security Guard</br>
                  <input type="checkbox" name="facilities[]" value="Tiles" />Tiles</br>
                </div>
                <div class="form-group">
                  <label for="conditions">Conditions<div class="input-group-addon" style="color: green"><i class="fa fa-adjust fa-spin"></i></div></label><br>
                    
                  <input type="checkbox" name="conditions[]" value="Must be Nonsmoker" />Must be Nonsmoker</br>
                  <input type="checkbox" name="conditions[]" value="Must be maintain of role of Room" />Must be maintain of role of Room</br>
                  <input type="checkbox" name="conditions[]" value="Before 11PM come back" />Before 11PM come back</br>
                  
                </div>
                <div class="form-group">
                  <label>Mobile</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-phone fa-spin fa-lg"></i></div>
                  <input type="number" name="mobile" class="form-control" value="{{ old('mobile')}}" required>
                  <p class="help-block">{{ ($errors->has('mobile')) ? $errors->first('mobile') : ''}}</p>
                </div>
                
                <div class="form-group">
                  <label for="city">Select City</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-cog fa-spin fa-lg"></i></div>
                  <select name="city" id="city" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose City==</option>
                    @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->city}}</option>
                    @endforeach
                  </select>
                  <p class="help-block">{{ ($errors->has('city')) ? $errors->first('city') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="thana">Select Thana</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-cog fa-spin fa-lg"></i></div>
                  <select name="thana" id="thana" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose Thana==</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('thana')) ? $errors->first('thana') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="ward">Select Ward</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-cog fa-spin fa-lg"></i></div>
                  <select name="ward" id="ward" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose Ward==</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('ward')) ? $errors->first('ward') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="address">Details Room Address</label>
                  <div class="input-group">
                    <div class="input-group-addon" style="color: green"><i class="fa fa-address-card fa-lg fa-spin"></i></div>
                  <textarea name="address" id="address" class="form-control" cols="30" rows="5"></textarea>
                  <p class="help-block">{{ ($errors->has('address')) ? $errors->first('address') : ''}}</p>
                </div>
                <button type="submit" class="btn btn-primary">Create</button>
              </form>
              <script type="text/javascript">
              $(document).ready(function(){
              $('select[name="city"]').on('change',function(){
              var cityid=$(this).val();
              if(cityid){
              $.ajax({
              url:'{{ url('')}}/cities/ajax/'+cityid,
              type:"GET",
              dataType:"json",
              success:function(data){
              $('select[name="thana"]').empty();
              $.each(data,function(key,value){
              $('select[name="thana"]').append('<option value="'+key+'">'+value+'</option>')
              });
              }
              });
              }else{
              $('select[name="thana"]').empty();
              }
              });
              $('select[name="thana"]').on('change',function(){
              var thanaid=$(this).val();
              console.log(thanaid);
              if(thanaid){
              $.ajax({
              url:'{{ url('')}}/thanas/ajax/'+thanaid,
              type:"GET",
              dataType:"json",
              success:function(data){
              $('select[name="ward"]').empty();
              $.each(data,function(key,value){
              $('select[name="ward"]').append('<option value="'+key+'">'+value+'</option>')
              });
              }
              });
              }else{
              $('select[name="ward"]').empty();
              }
              });
              });
              </script>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection