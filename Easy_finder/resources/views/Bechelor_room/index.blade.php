@extends('layouts.app')

@section('content')
@foreach($bechelor_room as $post)
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="box-title" style="text-align: center;font-size: 30px;color: white;background-color: #0321fd"><strong>{{$post->seat}} Roommate Wanted,From {{$post->date}} at {{$post->wards->ward}}</strong></h4>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <h4><span>Type</span></h4>
                                                        <h4><span>Gender</span></h4>
                                                        <h4><span>Religion</span></h4>
                                                        <h4><span>Status</span></h4>
                                                        <h4><span>City</span></h4>
                                                        <h4><span>Thana</span></h4>
                                                        <h4><span>Contact</span></h4>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <h4><span>{{$post->status}}</span></h4>
                                                        <h4><span>{{$post->gender}}</span></h4>
                                                        <h4><span>{{$post->religion}}</span></h4>
                                                        <h4><span>{{$post->married}}</span></h4>
                                                        <h4><span>{{$post->cities->city}}</span></h4>
                                                        <h4><span>{{$post->thanas->thana}}</span></h4>
                                                        <h4><span>{{$post->mobile}}</span></h4>
                                                    </div>
                                                </div>
                                                <h4 style="padding-top: 20px"><strong>Addeess:</strong> <span>{{$post->address}}</span></h4>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-5">
                                                        <h4><span></span></h4>
                                                        <h4><span>Room</span></h4>
                                                        <h4><span>Room Type</span></h4>
                                                        <h4><span>Room Rent</span></h4>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <h4><span class="pull-right"><a href="{{route('bechelor.booking',$post->id)}}" class="btn btn-success btn-sm">Now book</a></span></h4>
                                                        <h4><span>{{$post->room}}</span></h4>
                                                        <h4><span>{{$post->room_type}}</span></h4>
                                                        <h4><span>{{$post->room_rent}}</span></h4>
                                                    </div>
                                                </div>
                                                <h4 style="padding-top: 20px"><strong>Facilities:</strong> <span>{{$post->facilities}}</span></h4>
                                                <h4 style="padding-top: 20px"><strong>Conditions:</strong> <span>{{$post->conditions}}</span></h4>
                                            </div>
                                        </div>
                                        <p style="padding-top: 20px"><span>Created By : <a href="{{route('user_details.show', $post->user->id)}}">{{$post->user->name}}</a></span> <span class="pull-right">Created at: {{$post->created_at->toFormattedDateString()}}</span></p>
                                        <div class="card-header">
                                                <h4><strong>Comments</strong></h4>
                                        </div>
                                            @foreach($comments as $comment)
                                                    @if($post->id==$comment->bechelor_comment_id)
                                                    <p style="color: green"><span><a href="{{route('user_details.show', $comment->user->id)}}"><span style="color: green;"><b>{{$comment->user->name}}</b></span></a></span><span class="pull-right">{{$comment->created_at->toFormattedDateString()}}</span></p>
                                                    <p>
                                                        <span>{{ $comment->body }}</span>
                                                        <span class="pull-right">
                                                            @if(Auth::user()->email==$comment->user->email)
                                                            <span><a href="{{route('comment.edit', $comment->id)}}"><i class="fa fa-pencil"></i></a></span>
                                                            @endif
                                                        </span> 
                                                    </p>
                                                    @endif
                                            @endforeach
                                        <!-- <h4>Add comment</h4> -->
                                        <form method="post" action="{{ route('comment.store') }}">
                                            {{ csrf_field()}}
                                            <input type="hidden" name="bechelor_comment_id" value="{{ $post->id }}" />
                                            <label for="body">Add Comment</label>
                                                <textarea name="body" id="body" class="form-control" cols="30" rows="5"></textarea>
                                                <p class="help-block">{{ ($errors->has('body')) ? $errors->first('body') : ''}}</p>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-warning" value="submit">Comment</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card-body">
                                        @if($post->image!="")
                                        <div class="portfolio-item">
                                            <div class="portfolio-item-inner">
                                                <img class="img-responsive" src="{{asset($post->image)}}" alt="">
                                                    <div class="portfolio-info">
                                                        <a class="preview" href="{{asset($post->image)}}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @if(Auth::user()->email==$post->user->email)
                                        <p style="margin-top: 20px">
                                             <span class="btn btn-danger btn-sm"><a href="{{route('show_request.show_request', $post->id)}}" style="color: #fff"> Show Request</a></span>
                                            <span class="btn btn-warning pull-right"><a href="{{route('bechelor_room.edit', $post->id)}}" ><i class="fa fa-edit fa-lg" style="color: #fff"></i></a></span>
                                        </p>
                                        @endif
                                        <!-- <span class="pull-right"><a href="{{route('bechelor.booking',$post->id)}}" class="btn btn-primary btn-sm">Now book</a></span></p> -->
                                    </div>
                                </div>
                            </div>
                            <h4 style="text-align: center; color: green">===========Bechelor Roommate Wanted============</h4>
                            <div class="card-body"></div>
                        </div>
                    </div>
                </div>
                @endforeach
@endsection