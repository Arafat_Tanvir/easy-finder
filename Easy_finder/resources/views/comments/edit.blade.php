@extends('layouts.app')
@section('content')
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Edit Comment</strong>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 " style="margin-top: 120px">
                        </div>
                        <div class="col-sm-6">

                            <form method="post" action="{{ route('comment.update',$bechelor_comment->id)}}">
		                        {{ csrf_field()}}
		                        {{method_field('PUT')}}
		                        <div class="form-group">
					                  <textarea name="body" id="body" class="form-control" cols="30" rows="5">{{ $bechelor_comment->body }}</textarea>
					                  <p class="help-block">{{ ($errors->has('body')) ? $errors->first('body') : ''}}</p>
					            </div>
		                        <button type="submit" class="btn btn-warning" value="submit">Update</button>
		                    </form>
		                </div>
		                <div class="col-sm-3 " style="margin-top: 120px">
                        </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>
@endsection