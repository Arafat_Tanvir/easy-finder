@extends('layouts.app')
@section('content')
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @if((Session::get('message')))
            <div class="alert alert-success alert-dismissable">
              <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ Session::get('message')}}
              </div>
            </div>
        @endif
        <div class="card">
          <div class="card-header">
            <strong class="card-title mb-3">Profile</strong>
          </div>
          <div class="row">
            <div class="col-sm-4">
              
              @if(Auth::user()->image!="")
                    <img src="{{asset(Auth::user()->image)}}" alt="Card image cap" width="200px" height="280px">
              @endif
              <span class="pull-right" style="margin-top: 20px"><a href="{{route('profile.edit', Auth::user()->id)}}" class="btn btn-warning btn-sm">Edit</a></span>

            </div>
            <div class="col-sm-3">
              <h4><span>Name:</span></h4><hr>
              <h4><span>Email:</span></h4><hr>
              <h4><span>Gender:</span></h4><hr>
              <h4><span>National ID:</span></h4><hr>
              <h4><span>Religion</span></h4><hr>
              <h4><span>Date Of Birth</span></h4><hr>
              <h4><span>Contact</span></h4><hr>
              <h4><span>Address</span></h4><hr>
            </div>
            <div class="col-sm-5">
              <h4><span>{{Auth::user()->name}}</span></h4><hr>
              <h4><span>{{Auth::user()->email}}</span></h4><hr>
              <h4><span>{{Auth::user()->gender}}</span></h4><hr>
              <h4><span>{{Auth::user()->national_id}}</span></h4><hr>
              <h4><span>{{Auth::user()->religion}}</span></h4><hr>
              <h4><span>{{Auth::user()->date_of_birth}}</span></h4><hr>
              <h4><span>{{Auth::user()->mobile}}</span></h4><hr>
              <h4><span>{{Auth::user()->address}}</span></h4><hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection