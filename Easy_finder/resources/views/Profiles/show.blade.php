@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-8 offset-sm-2">
      <div class="card">
        <div class="card-header">
          <strong class="card-title pl-2">Profile</strong><span class="pull-right">Back</span>
        </div>
        <div class="card-body">
          <div class="mx-auto d-block">
            <div class="row">
              <div class="col-sm-4">
                <img  src="{{asset($users->image)}}" height="400px" width="500px" alt="">
              </div>
              <div class="col-sm-3">
                <h4><span>Name:</span></h4><hr>
                <h4><span>Email:</span></h4><hr>
                <h4><span>Gender:</span></h4><hr>
                <h4><span>National ID:</span></h4><hr>
                <h4><span>Religion</span></h4><hr>
                <h4><span>Date Of Birth</span></h4><hr>
                <h4><span>Contact</span></h4><hr>
                <h4><span>Address</span></h4><hr>
              </div>
              <div class="col-sm-5">
                <h4><span>{{$users->name}}</span></h4><hr>
                <h4><span>{{$users->email}}</span></h4><hr>
                <h4><span>{{$users->gender}}</span></h4><hr>
                <h4><span>{{$users->national_id}}</span></h4><hr>
                <h4><span>{{$users->religion}}</span></h4><hr>
                <h4><span>{{$users->date_of_birth}}</span></h4><hr>
                <h4><span>{{$users->mobile}}</span></h4><hr>
                <h4><span>{{$users->address}}</span></h4><hr>
              </div>
            </div>
          </div>
          <hr>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection