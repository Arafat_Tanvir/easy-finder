
@extends('layouts.app')
@section('content')
  <script type="text/javascript" src="{{ url('')}}/assets/js/jquery-3.2.1.min.js"></script>
  <div class="content">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8">
          @if((Session::get('message')))
          <div class="alert alert-success alert-dismissable">
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('message')}}
            </div>
          </div>
          @endif
          <div class="card">
            <div class="card-header" style="text-align: center; background-color: #0321fd; color: #ffffff">
              <strong>Bechelor Room Create/Post Form</strong>
            </div>
            <div class="card-body card-block">
                            <div class="col-sm-4 " style="margin-top: 120px">
                                @if($users->image!="")
                                <img src="{{asset($users->image)}}" alt="Card image cap" width="" height="">
                                @endif
                            </div>
                            <div class="col-sm-8">
                                <form class="form-horizontal" method="POST" action="{{ route('profile.update',$users->id) }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{method_field('PUT')}}
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Name</label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                                <input id="name" type="text" class="form-control" name="name" value="{{$users->name}}">
                                                @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                        <label for="gender" class="col-md-4 control-label">Gender</label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-king"></span>
                                                <input  type="radio" name="gender" value="male" @if($users->gender=='male') checked @endif>Male</div>
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-king"></span>
                                                <input type="radio" name="gender" value="female" @if($users->gender=='female') checked @endif>Female</div>
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-king"></span>
                                                <input type="radio" name="gender" value="other" @if($users->gender=='other') checked @endif>Other</div>
                                                @if ($errors->has('gender'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('gender') }}</strong>
                                                </span>
                                                @endif
                                           </div>
                                       </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                        <label for="date_of_birth" class="col-md-4 control-label">Date of Birth</label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></div>
                                            <input id="datepicker" type="text" class="form-control" name="date_of_birth" value="{{ $users->date_of_birth }}">
                                            @if ($errors->has('date_of_birth'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('date_of_birth') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                                        <label for="religion" class="col-md-4 control-label">Religion</label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-heart"></span></div>
                                            <select name="religion" id="religion" class="form-control">
                                                <option value="0" disabled="true" selected="true">===Choose Religion===</option>
                                                <option value="Islam" @if ($users->religion == "Islam")selected="selected" @endif >Islam</option>
                                                <option value="Hinduism" @if ($users->religion == "Hinduism")selected="selected" @endif >Hinduism</option>
                                                <option value="Buddhism" @if ($users->religion == "Buddhism")selected="selected" @endif >Buddhism</option>
                                                <option value="Christianity" @if ($users->religion == "Christianity")selected="selected" @endif >Christianity</option>
                                            </select>
                                            @if ($errors->has('religion'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('religion') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('national_id') ? ' has-error' : '' }}">
                                        <label for="national_id" class="col-md-4 control-label">National Id</label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-certificate"></span></div>
                                            <input id="national_id" type="number" class="form-control" name="national_id" value="{{ $users->national_id }}">
                                            @if ($errors->has('national_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('national_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                        <label for="mobile" class="col-md-4 control-label">Mobile</label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></div>
                                            <input id="mobile" type="number" class="form-control" name="mobile" value="{{ $users->mobile }}">
                                            @if ($errors->has('mobile'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('mobile') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                        <label for="image" class="col-md-4 control-label">Image</label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></div>
                                                <input id="image" type="file" class="form-control" name="image" value="{{ old('image') }}">
                                                @if ($errors->has('image'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label for="address" class="col-md-4 control-label">Address</label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></div>
                                                <textarea name="address" id="address" class="form-control" cols="30" rows="5" value="">{{ $users->address }}</textarea>
                                                @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                    </form>
                     </div>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection