<!DOCTYPE html>
<html>
    <head>
        <title>Transports Management System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{url('')}}/nav/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('')}}/nav/css/main.css">
    </head>
<body style="background-color: #eeffee">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $("#datepicker").datepicker({ dateFormat: "yy-mm-dd"}).val();
        })
    </script>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 well" style="margin-top: 100px">
                    <div class="hearder">
                        <h4 style="text-align: center; background-color: #eee;color: green; font-size: 30px"><b>Registration Information</b></h4>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="name" class="control-label">Name</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Enter Your Name" required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter Your Email" required>
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="password" class="control-label">Password</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Enter Your Password" required>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-md-12">
                                <label for="password-confirm" class="control-label">Confirm Password</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder=" Re-Passward" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="gender" class="control-label">Gender</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-king"></span><input type="radio" name="gender" value="male">Male</div>
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-queen"></span><input type="radio" name="gender" value="female">Female</div>
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-grain"></span><input type="radio" name="gender" value="other">Other</div>
                                    @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="date_of_birth" class="control-label">Date of Birth</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></div>
                                    <input id="datepicker" type="text" class="form-control" name="date_of_birth" value="{{ old('date_of_birth') }}" placeholder="Enter Your date_of_birth" required autofocus>
                                    @if ($errors->has('date_of_birth'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('religion') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="religion" class="control-label">Religion</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-heart"></span></div>
                                    <select name="religion" id="religion" class="form-control" required>
                                        <option value="0" disabled="true" selected="true">===Choose Religion===</option>
                                        <option value="Islam" >Islam</option>
                                        <option value="Hinduism" >Hinduism</option>
                                        <option value="Buddhism" >Buddhism</option>
                                        <option value="Christianity" >Christianity</option>
                                        <option value="Other" >Other</option>
                                    </select>
                                    @if ($errors->has('religion'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('religion') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('national_id') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="national_id" class="control-label">National Id</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-certificate"></span></div>
                                    <input id="national_id" type="number" class="form-control" name="national_id" value="{{ old('national_id') }}" placeholder="Enter Your National ID" required autofocus>
                                    @if ($errors->has('national_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('national_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="mobile" class="control-label">Mobile</label><div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></div>
                                
                                    <input id="mobile" type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Enter Your Phone Number" required autofocus>
                                    @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="image" class="control-label">Image</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-picture"></span></div>
                                    <input id="image" type="file" class="form-control" name="image" value="{{ old('image') }}" required autofocus>
                                    @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="address" class="control-label">Address</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></div>
                                    <textarea name="address" id="address" class="form-control" cols="30" rows="5" value="{{ old('address') }}"placeholder="Enter Your Address"  required autofocus></textarea>
                                    @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">
                        Register
                        </button>
                    </form>
                    <h4 class="pull-right">Already Have Account ? <a href={{url("/login")}} >Login </a></h4>
                    <script src="{{url('')}}/nav/js/jquery-3.2.1.min.js"></script>
                    <script src="{{url('')}}/nav/js/bootstrap.min.js"></script>
                    <script src="{{url('')}}/nav/js/main.js"></script>
                </div>
            </div>
        </div>
    </body>
</html>