<!DOCTYPE html>
<html>
  <head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/nav/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/nav/css/main.css">
  </head>
  <body style="background-color: #eeffee">
    <header>
    </header>
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-sm-offset-4 well" style="margin-top: 160px">
          <div class="hearder">
            <h4 style="text-align: center; background-color: #eee;color: green; font-size: 30px"><b>Login Information</b></h4>
          </div>
          <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field()}}
            <div class="form-group">
              <label for="email" class="form-control-label">E-Mail Address</label>
              <div class="input-group">
                <div class="input-group-addon" style="color: green"><span class="glyphicon glyphicon-envelope"></span></div>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="form-control-label">Password</label>
              <div class="input-group">
                <div class="input-group-addon" style="color: green"><span class="glyphicon glyphicon-lock"></div>
                <input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="form-control-label">
                <h5><span><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me</span> <span class="pull-right"><a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a></span></h5>
              </div>
            </div>
            <div class="form-group">
              <div class="form-control-label">
                <button type="submit" class="btn btn-primary">
                Login
                </button>
              </div>
            </div>
            <hr>
            Not register ? <a href={{url("/register")}} >click here </a> or go to <a href={{url("/")}}>Home</a>
          </form>
          <script src="{{url('')}}/nav/js/jquery-3.2.1.min.js"></script>
          <script src="{{url('')}}/nav/js/bootstrap.min.js"></script>
          <script src="{{url('')}}/nav/js/main.js"></script>
        </div>
      </div>
    </div>
  </body>
</html>