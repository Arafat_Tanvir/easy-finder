<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{url('')}}/assets/css/custom.css">
    <link rel="stylesheet" href="{{ url('')}}/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="{{ url('')}}/assets/css/style.css">

   <style>
    #weatherWidget .currentDesc {
        color: #ffffff!important;
    }
        .traffic-chart {
            min-height: 335px;
        }
        #flotPie1  {
            height: 150px;
        }
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;
        }

    </style>
</head>

<body>
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href=""><i class="menu-icon fa fa-laptop"></i>Dashboard</a>
                    </li>
                    <li class="menu-title">Profile</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Profile Details</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><a href="{{route('profile.index')}}">My Profile</a></li>
                        </ul>
                    </li>

                    <li class="menu-title">Bechelor Room</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Bechelor Details</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><a href="{{route('search')}}">Search</a></li>
                            <li><a href="{{route('bechelor_room.index')}}">All Bechelor Room</a></li>
                            <li><a href="{{route('bechelor_room.create')}}">Create Post</a></li>
                            <li><a href="{{route('bechelor_post.by_user_id', Auth::user()->id)}}">My Post</a></li>
                            <li><a href="{{route('bechelor_booking.user_booking', Auth::user()->id)}}">My Booking</a></li>
                        </ul>
                    </li>

                    <li class="menu-title">Family Room </li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Family Room Details</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><a href="{{route('bechelor_room.index')}}">Bechelor Room</a></li>
                            <li><a href="{{route('bechelor_booking.user_booking', Auth::user()->id)}}">My Post</a></li>
                        </ul>
                    </li>


                    <li class="menu-title">Sublet Room</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Sublet Details</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><a href="{{route('bechelor_room.index')}}">Bechelor Room</a></li>
                            <li><a href="{{route('bechelor_booking.user_booking', Auth::user()->id)}}">My Post</a></li>
                        </ul>
                    </li>
                    

                    <li class="menu-title">Extras</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <div id="right-panel" class="right-panel">
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    </a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="user-area dropdown float-right">
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{asset(Auth::user()->image)}}" alt="" height="40px" width="40" class="user-avatar rounded-circle"><span class="caret"></span>
                                </a>
                                <ul class="user-menu dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
        </header>
        @yield('content')
    </div>
    <div class="clearfix"></div>
    <div id="right-panel" class="right-panel">
        <footer class="site-footer">
            <div class="row">
                <!-- <div class="col-sm-12">
                    <div class="copyright-area text-center">
                        <h1>All Rights Reserved By &copy; LICT</h1>
                    </div>
                    <div class="footer-inner bg-white">
                        <div class="row">
                            <div class="col-sm-6">
                                <div style="text-align: center;">
                                  <img src="../public/images/bechelor_room/Bechelor_Room-3.jpg" alt="" width="120px" height="120px">
                                </div>
                                 <div class="tab-content" style="font-size: 20px; text-align: center;">
                                    <div class="tab-pane fade in active">
                                        <p class="narrow text-center">
                                          Submitted By : Saisha Tanju Anika
                                          <br>
                                          Email : saishatanjuanki15@gmail.com
                                          <br>
                                          University : Chittagong City Corporation Kaiser-Niloufer College
                                        </p>
                                        <p class="text-center">
                                            <a href="https://www.facebook.com/KamrulHasanKutubi" class="btn btn-info btn-outline-rounded green"><i class="fa fa-facebook"></i></a>

                                            <a href="https://github.com/settings/profile" class="btn btn-success btn-outline-rounded green"><i class="fa fa-github"></i> </a>

                                            <a href="https://www.youtube.com/channel/UC0CjuzhW7i7C0dbts3wzZaA" class="btn btn-primary btn-outline-rounded green"><i class="fa fa-youtube"></i></a>

                                            <a href="https://plus.google.com/u/0/112314462459005413454" class="btn btn-warning btn-outline-rounded green"><i class="fa fa-google"></i></a>
                                        </p>
                                  </div>
                               </div>
                            </div>
                            <div class="col-sm-6">

                                <div style="text-align: center;">
                                  <img src="../public/images/bechelor_room/Bechelor_Room-4.jpg" class="img-circle" alt="" width="120px" height="120px">
                                </div>

                                 <div class="tab-content" style="font-size: 20px; text-align: center;">
                                    <div class="tab-pane fade in active">
                                        <p class="narrow text-center">
                                          Submitted By : Kamrul Hasan Kutubi
                                          <br>
                                          Email : kamrulhasancse15@gmail.com
                                          <br>
                                          University : International islamic University Chittagong
                                        </p>
                                        <p class="text-center">
                                            <a href="https://www.facebook.com/KamrulHasanKutubi" class="btn btn-info btn-outline-rounded green"><i class="fa fa-facebook"></i></a>

                                            <a href="https://github.com/settings/profile" class="btn btn-success btn-outline-rounded green"><i class="fa fa-github"></i> </a>

                                            <a href="https://www.youtube.com/channel/UC0CjuzhW7i7C0dbts3wzZaA" class="btn btn-primary btn-outline-rounded green"><i class="fa fa-youtube"></i></a>

                                            <a href="https://plus.google.com/u/0/112314462459005413454" class="btn btn-warning btn-outline-rounded green"><i class="fa fa-google"></i></a>
                                        </p>
                                  </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </footer>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="{{url('')}}/assets/js/main.js"></script>

    <!--  Chart js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

    <!--Chartist Chart-->
    <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
    <script src="{{url('')}}/assets/js/init/weather-init.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
    <script type="text/javascript" src="{{url('')}}/assets/js/init/fullcalendar-init.js"></script>
</body>
</html>
