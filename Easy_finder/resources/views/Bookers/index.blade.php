@extends('layouts.app')
@section('content')

@foreach($booking as $post)
 <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="box-title">
                                    <h4 style="text-align: center;color:#fff;font-size: 40px;background-color: #0321fd;">Booking Information</h4>
                                    <span class="pull-right" style="margin-top: 5px"><input type="button" value="Print" onClick="window.print()"></span>
                                </h4>
                            </div>
                            <div class="row">
                                <div class="col-lg-6" >
                                    <div class="card-body">
                                        @if($post->bechelor_bookings->image!="")
                                        <div class="portfolio-item">
                                            <div class="portfolio-item-inner">
                                                <img class="img-responsive" src="{{asset($post->bechelor_bookings->image)}}" alt="">
                                                <div class="portfolio-info">
                                                    <a class="preview" href="{{asset($post->bechelor_bookings->image)}}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        @endif

                                        @if(Auth::user()->email==$post->bechelor_bookings->user->email)
                                            <form method="post" action="{{ route('bechelor_confirm.completed',$post->id) }}">
                                            {{ csrf_field()}}
                                            <input type="hidden" name="bechelor_room_id" value="{{$post->bechelor_bookings->id}}">
                                                 <button type="submit" class="btn btn-primary btn-sm" style="margin-top: 5px">{{$post->status==true? 'Mark Pending':'Mark Confirm'}}</button>
                                            </form>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card-body">
                                        <div class="col-sm-12">
                                            <h4>Dear,<a href="{{route('user_details.show', $post->user->id)}}">{{$post->user->name}}</a><span class="pull-right" style="text-align: center;color: red">Date: {{$post->created_at->toFormattedDateString()}}</span></h3><hr>
                                            <h4>Congratulations,You have to request {{$post->seat}} seat for you.Now it's pendding,please contact match insearch and quary all information on this Seat.Seat is choosen by you please advance your seat money.Wait until confirm. <hr>
                                            <span>My name is {{$post->bechelor_bookings->user->name}}.I am match insearch.If Any Quary {{$post->bechelor_bookings->user->mobile}}.</span></h4><hr>

                                            <span>Total Seat</span><span class="pull-right">{{$post->seat}}</span><br>
                                            <span>Total Seat Rent</span><span class="pull-right">{{$post->room_rent}}</span>


                                            <p style="margin-top: 20px">
                                            <span class="btn btn-success btn-sm">{{$post->status == false?'Pendding':'confirm'}}</span>
                                            <span class="pull-right btn btn-warning btn-sm"><a href="{{route('room_details.room_details',$post->bechelor_bookings->id)}}">Room Details</a></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body"></div>
                        </div>
                    </div>
                </div>
@endforeach
@endsection