
@extends('layouts.app')
@section('content')
  <script type="text/javascript" src="{{ url('')}}/assets/js/jquery-3.2.1.min.js"></script>
  <div class="content">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 offset-xs-3 offset-sm-3 offset-md-3">
          @if((Session::get('message')))
          <div class="alert alert-success alert-dismissable">
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('message')}}
            </div>
          </div>
          @endif
          <div class="card">
            <div class="card-header" style="text-align: center; background-color: #0321fd; color: #ffffff">
              <strong>Bechelor Room Create/Post Form</strong>
            </div>
            <div class="card-body card-block">
                <form role="form" action="{{ route('booking.store')}}" method="post" enctype="multipart/form-data">
	                {{ csrf_field()}}
	                <input type="hidden" name="bechelor_room_id" value="{{$bechelor_booking->id}}">
	                <div class="form-group">
		                <label>How Many Seats You Can Comform</label>
		                <select name="seat" id="seat" class="form-control" required>
			                <option value="0" disabled="true" selected="true">===Choose Seat===</option>
			                <option value="1">1 Seat</option>
			                <option value="2" >2 Seat</option>
			                <option value="3" >3 Seat</option>
			                <option value="4" >4 Seat</option>
			                <option value="5" >5 Seat</option>
			                <option value="6" >6 Seat</option>
			                <option value="7" >7 Seat</option>
			                <option value="8" >8 Seat</option>
		                  </select>
		                  <small style="color: red">Please Select less than or equal to {{$bechelor_booking->seat}} Seats</small>
		                  <p class="help-block">{{ ($errors->has('seat')) ? $errors->first('seat') : ''}}</p>
	                </div>
	                <button type="submit" class="btn btn-primary">Book Confirm</button>
	            </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection