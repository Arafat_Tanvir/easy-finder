<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBechelorRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bechelor__rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->string('status');
            $table->string('gender');
            $table->string('religion');
            $table->string('married');
            $table->string('room');
            $table->String('seat');
            $table->string('room_type');
            $table->float('room_rent', 8, 2);
            $table->string('image');
            $table->text('facilities');
            $table->text('conditions');
            $table->string('mobile');
            $table->integer('city')->unsigned();
            $table->foreign('city')->references('id')->on('cities');
            $table->integer('thana')->unsigned();
            $table->foreign('thana')->references('id')->on('thanas');
            $table->integer('ward')->unsigned();
            $table->foreign('ward')->references('id')->on('wards');
            $table->text('address');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bechelor__rooms');
    }
}
