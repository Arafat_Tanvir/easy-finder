<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBechelorBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bechelor_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seat');
            $table->float('room_rent', 8, 2);
            $table->boolean('status')->default(false);
            $table->integer('bechelor_room_id')->unsigned();
            $table->foreign('bechelor_room_id')->references('id')->on('bechelor__rooms');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bechelor_bookings');
    }
}
