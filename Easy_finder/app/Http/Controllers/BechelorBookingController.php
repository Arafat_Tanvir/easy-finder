<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Bechelor_Room;
use App\BechelorBooking;
class BechelorBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $booking=BechelorBooking::orderBy('id','desc')->get();
        //dd($booking);
        return view('Bookers.index',compact('booking'));
    }

    public function booking(Request $request, $id)
    {
        $input= $request->all();
        $bechelor_booking = Bechelor_Room::findOrFail($id);
        //dd($bechelor_booking);
        return view('Bookers.create',compact('bechelor_booking'));
    }




     public function completed(Request $request,$id){

        $complete = BechelorBooking::find($id);
        //dd($complete);
        $bechelor_booking = Bechelor_Room::findOrFail($request->bechelor_room_id);
        $available_seat = $bechelor_booking->seat;
        $booking=$complete->seat;
        //$update_seat = $available_seat - $booking;

        // if($booking<=$available_seat){
        //     $seat_confirm->update();
        // }else{
        //     return back();
        // }
        if($complete->status==0){

            $update_seat = $available_seat - $booking;
            $bechelor_booking->seat = $update_seat;

            if($booking<=$available_seat){
                $bechelor_booking->update();
                $complete->status = 1;
                $complete->update();
            }else{
                return back();
            }
        }else{
            $update_seat = $available_seat + $booking;
            $bechelor_booking->seat = $update_seat;
            $bechelor_booking->update();
            $complete->status = 0;
            $complete->update();
        }
        

        return redirect()->back()->with('message', 'Confirm Successfully');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function user_booking($id){
        $booking = BechelorBooking::where('user_id', $id)
        ->orderBy('id','desc')
        ->get();
        //dd($booking);
        return view('Bookers.index',compact('booking'));
    }
    //show request
    public function show_request($id){
        $booking = BechelorBooking::where('bechelor_room_id', $id)
        ->orderBy('id','desc')
        ->get();
        //dd($booking);
        return view('Bookers.index',compact('booking'));
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "seat"=>"required"
        ]);
        $input= $request->all();
        //dd($input);
        $input['user_id'] = Auth::user()->id;
        $bechelor_booking = Bechelor_Room::findOrFail($request->bechelor_room_id);
        $booking=$input['seat'];
        $available_seat=$bechelor_booking->seat;

        $current_amount=$bechelor_booking->room_rent;
        $total_amount=$current_amount*$booking;
        $input['room_rent'] = $total_amount;
        $user_id=$input['user_id'];
        $bechelor_room_id=$input['bechelor_room_id'];
        //dd($bechelor_room_id);
        if($booking<=$available_seat){
            $result=BechelorBooking::create($input);
            if($result){
                return redirect()->to('bechelor_booking/user_booking/'.$user_id)->with('message','Booking Request Successfully,Contact the match inserch,wait until confirm');
            }else{
                return beck()->with('message','Some Error Occar');
            }
        }else{
            return redirect()->to('bechelor/'.$bechelor_room_id.'/booking')->with('message','please select lessthen or equal to '.$available_seat. ' seat');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
