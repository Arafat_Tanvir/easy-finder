<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Thana;
use App\Bechelor_Room;
use App\BechelorComment;
use App\User;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //MY POST 
    public function by_user_id($id){
        //dd($id);
        $bechelor_room = Bechelor_Room::where('user_id', $id)->get();
        //dd($bechelor_room);
        $thanas = Thana::all();
        $comments=BechelorComment::all();
        return view('Bechelor_room.index', compact('bechelor_room', 'thanas','comments'));
    }

    //
    public function index()
    {
        return view('Profiles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function upload($file){
        $name =time().$file->getClientOriginalName();
        $filename=$name;
        $current = public_path('profile_images/');
        $file->move($current, $filename);
        return 'profile_images/'.$filename;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::findOrFail($id);
        return view('Profiles.edit',compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     "name"=>"required",
        //     "email"=>"required",
        //     "password"=>"required",
        //     "gender"=>"required",
        //     "date_of_birth"=>"required",
        //     "religion"=>"required",
        //     "national_id"=>"required",
        //     "mobile"=>"required",
        //     "image"=>"required",
        //     "address"=>"required"
        // ]);
        $users = User::findOrFail($id);
        $input =$request->all();
        if(isset($input['image'])){
            $input['image'] = $this->upload($input['image']);
        }
        $result=$users->update($input);
        if($result){
            return redirect('profile')->with('message','Profile update Successful');
        }else{
            return beck()->with('message','Some Error Occar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
