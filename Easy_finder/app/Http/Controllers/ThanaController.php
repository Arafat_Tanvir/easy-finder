<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Thana;
use App\City;
use App\BechelorComment;
use App\Bechelor_Room;

class ThanaController extends Controller
{

    public function search()
    {
        $cities=City::all();
        return view('Bechelor_room.search', compact('cities'));
    }

    public function search_post(Request $request)
    {
        $search = new Thana();
        $city_id=$search->city = $request->city;
        $thana_id=$search->thana = $request->thana;
        $ward_id=$search->ward = $request->ward;
        $bechelor_room = Bechelor_Room::where('city', $city_id)
        ->where('thana',$thana_id)
        ->where('ward',$ward_id)
        ->get();
        $cities=City::all();
        $thanas = Thana::all();
        $comments=BechelorComment::all();
        $result=count($bechelor_room);

        if($result>0){
            return view('Bechelor_room.index', compact('cities','bechelor_room','comments','thanas'));
        }else{
            return redirect('search')->with('message','No Seat or Room Availbale');
        }

        
    }

}
