<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'gender' => 'required|max:255',
            'date_of_birth' => 'required',
            'religion' => 'required',
            'national_id' => 'required|max:255',
            'mobile' => 'required|size:11',
            'image' => 'required',
            'address' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //var_dump($data);
        $request = request();
        $file=$request->file("image");
        $profileImageSaveAsName = time() . Auth::id() . "-profile." .$file->getClientOriginalExtension();
        $upload_path = 'profile_images/';
        $ext = $upload_path . $profileImageSaveAsName;
        $success = $file->move($upload_path, $profileImageSaveAsName);



        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'gender' => $data['gender'],
            'date_of_birth' => $data['date_of_birth'],
            'religion' => $data['religion'],
            'national_id' => $data['national_id'],
            'mobile' => $data['mobile'],
            'address' => $data['address'],
            'image'=>$ext,
        ]);
    }
}
