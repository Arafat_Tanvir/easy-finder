<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Bechelor_Room;
use App\BechelorComment;

class BechelorCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input= $request->all();
        $input['user_id'] = Auth::user()->id;
        $bechelor_comment_id=$input['bechelor_comment_id'];
        //dd($bechelor_comment_id);
        $result=BechelorComment::create($input);
        if($result){
            return redirect()->to('room_details/room_details/'.$bechelor_comment_id);
        }else{
            return beck()->with('message','Some Error Occar');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bechelor_comment = BechelorComment::findOrFail($id);
        //dd($bechelor_comment);
        return view('Comments.edit',compact('bechelor_comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bechelor_comment = BechelorComment::findOrFail($id);
        $input =$request->all();
        //dd($input);
        
        $bechelor_comment->update($input);
        return redirect('bechelor_room');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
