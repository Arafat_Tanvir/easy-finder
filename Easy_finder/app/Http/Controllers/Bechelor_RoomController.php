<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\City;
use App\Ward;
use App\Thana;
use Illuminate\Support\Facades\Auth;
use App\Bechelor_Room;
use App\BechelorComment;
use DB;

class Bechelor_RoomController extends Controller
{

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bechelor_room=Bechelor_Room::orderBy('id','desc')->get();
        $comments=BechelorComment::all();
        //dd($comments);
        return view('Bechelor_room.index',compact('bechelor_room','comments'));
    }

    public function room_details($id){
        $bechelor_room = Bechelor_Room::where('id', $id)
        ->orderBy('id','desc')
        ->get();
        $comments=BechelorComment::all();
        //dd($bechelor_room);
        return view('Bechelor_room.index',compact('bechelor_room','comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities=City::all();
        return view('Bechelor_room.create',compact('cities'));
    }

    public function cities_ajax($id)
    {
        $Thanas=DB::table("Thanas")
        ->where("city_id",$id)
        ->pluck("thana","id");
        return json_encode($Thanas);
    }

    public function thanas_ajax($id)
    {
        $wards=DB::table("wards")
        ->where("thana_id",$id)
        ->pluck("ward","id");
        return json_encode($wards);
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input= $request->all();
        $input['user_id'] = Auth::user()->id;

        $input['facilities'] = implode(',', $input['facilities']);
        $input['conditions'] = implode(',', $input['conditions']);

        if(isset($input['image'])){
            $input['image'] = $this->upload($input['image']);
        }else{
            $input['image'] = 'image/default.jpg';
        }
        //dd($input);
        $user_id=$input['user_id'];

        $result=Bechelor_Room::create($input);
        if($result){
            return redirect()->to('bechelor_post/by_user_id/'.$user_id)->with('message','Create Successfully');
        }else{
            return beck()->with('message','Some Error Occar');
        }

    }

    public function upload($file){
        //$extension = $file->getClientOrginalExtension();
        $name =time().$file->getClientOriginalName();
        $filename=$name;
        $current = public_path('image_bechelor_room/');
        $file->move($current, $filename);
        return 'image_bechelor_room/'.$filename;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bechelor_room = Bechelor_Room::findOrFail($id);
        $comments=BechelorComment::all();
        return view('assistants.show',compact('bechelor_room','comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bechelor_rooms = Bechelor_Room::findOrFail($id);
        $facilities = explode(",", $bechelor_rooms->facilities);
        $conditions = explode(",", $bechelor_rooms->conditions);
        $cities=City::all();
        return view('bechelor_room.edit',compact('bechelor_rooms','cities','facilities','conditions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bechelor_rooms = Bechelor_Room::findOrFail($id);
        $input =$request->all();
        if(isset($input['image'])){
            $input['image'] = $this->upload($input['image']);
        }
        $input['facilities'] = implode(',', $input['facilities']);
        $input['conditions'] = implode(',', $input['conditions']);
        $result=$bechelor_rooms->update($input);
        if($result){
            return redirect('bechelor_room')->with('message','Update Successfully');
        }else{
            return beck()->with('message','Some Error Occar');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
