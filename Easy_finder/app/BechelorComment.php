<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BechelorComment extends Model
{
    protected $table="bechelor_comments";
    protected $fillable= ['body', 'bechelor_comment_id', 'user_id'];
    
    public function user(){
    	return $this->belongsTo('App\User');
    }
}
