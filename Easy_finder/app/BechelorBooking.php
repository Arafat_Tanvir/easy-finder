<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BechelorBooking extends Model
{
    protected $table="bechelor_bookings";
    protected $fillable= ['seat', 'room_rent','status','bechelor_room_id', 'user_id'];
    
    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function bechelor_bookings()
    {
        return $this->belongsTo('App\Bechelor_Room', 'bechelor_room_id');
    }

    
}
