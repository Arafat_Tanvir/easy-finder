<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bechelor_Room extends Model
{
	protected $table="bechelor__rooms";
    protected $fillable= ['date', 'status', 'gender', 'religion', 'married', 'room','seat', 'room_type', 'room_rent', 'image', 'facilities', 'conditions','mobile', 'city', 'thana', 'ward', 'address', 'user_id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }


    public function cities()
    {
        return $this->belongsTo('App\City', 'city');
    }

    public function thanas(){
    	return $this->belongsTo('App\Thana','thana');
    }

    public function wards(){
    	return $this->belongsTo('App\Ward','ward');
    }
}
